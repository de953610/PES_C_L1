#include<stdio.h>


struct node {
  char name[500];
  int age;
  struct node *next;
};


/* function to print Nth name from linked list*/ 
void printName(struct node *hd , char nme[])
{
    struct node *temp;
    int i=1;
    int j,term=0;
    
    for(i=0 ; i<strlen(nme) ; i++)
    {
        if(nme[i]==' ')
            break;
    }
    
    i++;
    while(i<strlen(nme))
    {
        term = term*10 + (nme[i]-'0');
        i++;
    }
    
    
    temp=hd;
   
    
    temp=hd;
    i=1;
    while(temp != NULL && i != term)
    {
        
        temp = temp->next;
        i++;
        
    }
    
    if(i==term)
        printf("%s %d\n",temp->name,temp->age);
    
  
    
}

/* Remove Nth name from linked list */

void removeName(struct node **hd , char nme[])
{
    
    struct node *start,*temp;
    
    start = *hd;
    
    int j,term=0,i;
    
    for(i=0 ; i<strlen(nme) ; i++)
    {
        if(nme[i]==' ')
            break;
    }
    
    i++;
    while(i<strlen(nme))
    {
        term = term*10 + (nme[i]-'0');
        i++;
    }
    
    
    if(term == 1)
    {
        temp = *hd;
        (*hd) = (*hd)->next;
        
        free(temp);
    }
    else
    {
        i=2;
        while(start->next != NULL && i !=term)
        {
            start = start->next;
        }
        
        if(i == term)
        {
            temp = start->next;
            start->next = start->next->next;
            free(temp);
        }
    }
    
    
    
}


/* Insert node at first place in linked list */
void insertAtFirst(struct node **hed , char nm[], int iage )
{
    struct node *temp,*tp;
    
    temp = (struct node*)malloc(sizeof(struct node));
    
    tp = *hed;
    temp->age = iage;
    strcpy(temp->name , nm);
    temp->next = tp;
    
    *hed = temp;
}



/*Insert name in linked list*/

void insertName(struct node **hd , char nme[])
{
    
    char cname[200];
    int i,j,iage;
    
    for(i=0 ; i<strlen(nme) ; i++)
    {
        if(nme[i]==' ')
            break;
    }
    i++;
    j=0;
    for( ; i<strlen(nme) ; i++)
    {
        if(nme[i]==' ')
        {
            cname[j]='\0';
            i++;
            break;
        }
        cname[j++]=nme[i];
    }
    
    iage = 0;
    while(i<strlen(nme))
    {
        iage = iage*10 + (nme[i]-'0');
        i++;
    }
    
  
    
    
    
    struct node *temp,*hemp,*start;
    
    
    if((*hd) == NULL || (*hd)->age > iage)
    {
 
        insertAtFirst(hd, cname, iage);
    }
    else
    {
 
         start = *hd;
        while(start->next != NULL && iage > start->age)
        {
 
            start = start->next;
        }
        
        if(start->next == NULL)
        {
 
            temp = (struct node*)malloc(sizeof(struct node));
            
            temp->age = iage;
            strcpy(temp->name , cname);
            temp->next = NULL;
            
            start->next = temp;
            
 
        }
        else
        {
 
            temp = (struct node*)malloc(sizeof(struct node));
            strcpy(temp->name , cname);
            temp->age = iage;
            hemp = start->next;
            temp->next = hemp;
            start->next = temp;
            
            
        }
    }
    
    
    
    
}


void display()
{
    printf("Only four operations are permissible : \n\n");
    printf(" Insert name age (to insert name and age in linked list)\n\n");
    printf(" Print N (to print the Nth name in linked list)\n\n");
    printf(" Remove N (to remove the Nth name in linked list) \n\n");
    printf(" Stop ( to stop the instruction) \n\n");
    
    printf("Don't forget to give instruction (stop) to come out from the loop\n\n");
    
}



/*Main function*/

int main()
{
    char input[20000];
    int a;
    
    struct node *head=NULL;
    display();
    
    while(1)
    {
        
        printf( "Enter Data : ");
        gets(input);
    
        a = (int) input[0];
        
        if(a<96)
            a+=32;
        
        
        switch(a)
        {
            case 105:
                        insertName(&head,input);
                        break;
            case 112:
                        printName(head,input);
                        break;
            case 114:
                        removeName(&head,input);
                        break;
            case 115:
                        return 0;
                        break;
                        
            default : printf("\n\n * Please enter input as specified \n\n");
                      display();
            
        
        }
        
    }
    return 0;
}
